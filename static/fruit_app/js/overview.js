function get_overview_data() {
    data = {
        'start_date':$('#id_start_date').val(),
        'end_date':$('#id_end_date').val(),
    }
    // #TODO handle edge cases, start date > end date
    $.ajax({
            type: 'GET',
            //TODO change url make standard
            url: "/ajax_overview",
            data: data,
            success: function (response) {
                $('#overview').html(response)
            },
            error: function (response) {
                console.log(response)
            }
    })
}
