from django.shortcuts import render
from django import http, urls
from django.views.generic import list, edit, DetailView, DeleteView
from django.db.models import Sum, F

from datetime import datetime
import yaml
import csv
from openpyxl import load_workbook

from . import forms
from . import models
from fruit_seller.settings import MEDIA_ROOT

# Create your views here.


def home(request):
    return render(request, "home.html", {})


class RegisterView(edit.CreateView):
    form_class = forms.RegisterationForm
    success_url = urls.reverse_lazy("home")
    template_name = "registration/registration.html"


def upload(request):
    return render(request, "fruit_app/upload.html")


class UploadMultiple(edit.FormView):
    form_class = forms.UploadMultipleSpreadsheetForm
    template_name = "fruit_app/upload_multiple.html"
    success_url = urls.reverse_lazy("upload")

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        files = request.FILES.getlist("file")
        if form.is_valid():
            for f in files:
                process_and_save_file_data(
                    f,
                    request.user
                )
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


def upload_single(request):
    if request.method == "POST":
        uploadForm = forms.UploadSpreadsheetForm(
            request.POST,
            request.FILES,
        )
        if uploadForm.is_valid():
            process_and_save_file_data(
                uploadForm.cleaned_data.get("file"),
                request.user
            )

    uploadForm = forms.UploadSpreadsheetForm()
    return render(
        request,
        "fruit_app/upload_single.html",
        {"form": uploadForm}
    )


# TODO kwargs
def process_and_save_file_data(file, user):
    fruit_file_instance_data = file.name.split("-")
    week_date = datetime.date(
        fruit_file_instance_data[0],
        fruit_file_instance_data[1],
        fruit_file_instance_data[2]
    )
    city_name = " ".join(fruit_file_instance_data[3:])
    city_instance = models.City.objects.get(name=city_name.split(".")[0])
    fruit_file_instance = models.FruitFile(
        file=file,
        user=user,
        week_date=week_date,
        shop=models.Shop.objects.get(city=city_instance),
    )
    fruit_file_instance.save()
    filepath = fruit_file_instance.file.name
    wb = load_workbook(filename=MEDIA_ROOT + filepath, read_only=True)
    save_workbook_data(wb, fruit_file_instance)


def save_workbook_data(wb, fruit_file_instance):
    for sheet in wb.worksheets:
        rows = sheet.max_row
        for row in range(2, rows + 1):
            if sheet.title == "by fruit":
                fruit_stats_model = models.FruitStatistics(
                    fruit_file=fruit_file_instance,
                    fruit=models.Fruit.objects.get(
                        name=sheet.cell(row=row, column=1).value
                    ),
                    units_bought=sheet.cell(row=row, column=2).value,
                    cost_per_unit=sheet.cell(row=row, column=3).value,
                    units_sold=sheet.cell(row=row, column=4).value,
                    price_per_unit=sheet.cell(row=row, column=5).value,
                    units_wastage=sheet.cell(row=row, column=6).value,
                )

                fruit_stats_model.save()
            elif sheet.title == "overheads":
                overhead_model = models.Overheads(
                    fruit_file=fruit_file_instance,
                    personnel_cost=sheet.cell(row=row, column=1).value,
                    premises_cost=sheet.cell(row=row, column=2).value,
                    other_overheads=sheet.cell(row=row, column=3).value,
                )
                overhead_model.save()


class ListShopView(list.ListView):
    model = models.Shop
    template_name = "fruit_app/shop_list.html"


class CreateShopView(edit.CreateView):
    model = models.Shop
    template_name = "fruit_app/create_shop.html"
    fields = ["name", "city", "address", "first_opened", "is_active"]
    success_url = urls.reverse_lazy("shop_list")


class EditShopView(edit.UpdateView):
    model = models.Shop
    template_name = "fruit_app/edit_shop.html"
    fields = ["name", "city", "address", "first_opened", "is_active"]
    success_url = urls.reverse_lazy("shop_list")


class ViewShopView(DetailView):
    model = models.Shop
    template_name = "fruit_app/view_shop.html"


class DeleteShopView(DeleteView):
    model = models.Shop
    template_name = "fruit_app/delete_shop.html"
    success_url = urls.reverse_lazy("shop_list")


def initialize_fruit_city_data(request):
    if request.method == "POST":
        is_data_present_in_db = False
        if request.POST["update"] == "True":
            with open("raw_data.yaml", "r") as stream:
                loaded_stream = yaml.safe_load(stream)
                for model in loaded_stream:
                    for model_values in loaded_stream[model]:
                        if model == "cities":
                            city_model = models.City(name=model_values)
                            city_model.save()
                        elif model == "fruit":
                            fruit_model = models.Fruit(
                                name=model_values[0], unit=model_values[1]
                            )
                            fruit_model.save()
            is_data_present_in_db = True

    elif request.method == "GET":
        is_data_present_in_db = False
        city = models.City.objects.all()
        if city:
            is_data_present_in_db = True

    return render(
        request,
        "fruit_app/initialize_fruit_city_data.html",
        {"is_data_present_in_db": is_data_present_in_db},
    )


def custom_404_view(request, exception):
    return render(request, "404.html", {})


class FileListView(list.ListView):
    model = models.FruitFile
    template_name = "fruit_app/file_list.html"


class FileDetailView(DetailView):
    model = models.FruitFile
    template_name = "fruit_app/file_detail.html"
    slug_url_kwarg = "slug_from_url"
    slug_field = "slug"

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data["raw_data"] = {
            "weekly_data": models.FruitStatistics.objects.filter(
                fruit_file=self.object.id
            ),
            "overhead_data": models.Overheads.objects.filter(
                fruit_file=self.object.id
            ),
        }
        return context_data


def csv_export(request):
    response = http.HttpResponse(
        content_type="text/csv",
        headers={
            "Content-Disposition": 'attachment; filename="somefilename.csv"'
        },
    )

    query = models.FruitStatistics.objects.values(
        "fruit_file__week_date"
    ).annotate(
        total_sales=Sum(F("units_sold") * F("price_per_unit"))
    )

    writer = csv.writer(response)
    writer.writerow(["Week Date", "Total Sales"])
    for row in query:
        writer.writerow([row["fruit_file__week_date"], row["total_sales"]])

    return response


def overview(request):
    if request.method == "GET":
        context = {"form": forms.OverviewFormFilters}
        return render(request, "fruit_app/overview.html", context=context)


def ajax_overview(request):
    if request.method == "GET":
        start_date = request.GET["start_date"]
        end_date = request.GET["end_date"]

        result = {}
        week_offset = 0

        week_wise_fruits_data_query = models.FruitStatistics.objects
        if start_date != "":
            week_wise_fruits_data_query = week_wise_fruits_data_query.filter(
                fruit_file__week_date__gte=start_date
            )

        if end_date != "":
            week_wise_fruits_data_query = week_wise_fruits_data_query.filter(
                fruit_file__week_date__lte=end_date
            )

        week_wise_fruits_data = (
            week_wise_fruits_data_query.order_by(
                "fruit_file__week_date",
                "fruit__name"
            )
            .values(
                "fruit_file__week_date",
                "fruit__name"
            )
            .annotate(
                total_sales=Sum(F("units_sold") * F("price_per_unit"))
            )
        )

        # TODO logic can be improved for final data
        # TODO change row date format
        for subset in week_wise_fruits_data:
            if week_offset % 48 == 0:
                week_date = str(subset["fruit_file__week_date"])
                result[week_date] = {}
            # TODO round the result to 2
            result[week_date][subset["fruit__name"]] = (
                result[week_date].get(
                    subset["fruit__name"], 0
                ) + subset["total_sales"]
            )
            week_offset += 1

        columns = models.Fruit.objects.order_by("name").all()
        context = {"result": result, "columns": columns}
        return render(request, "fruit_app/ajax_overview.html", context=context)
