# Generated by Django 4.1 on 2022-09-07 11:41

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("fruit_app", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="fruitstatistics",
            old_name="city",
            new_name="shop",
        ),
        migrations.RenameField(
            model_name="overheads",
            old_name="city",
            new_name="shop",
        ),
        migrations.RenameModel(
            old_name="City",
            new_name="Shop",
        ),
        migrations.CreateModel(
            name="Store",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(blank=True, max_length=20, null=True)),
                ("address", models.CharField(blank=True, max_length=100, null=True)),
                (
                    "first_opened",
                    models.DateField(verbose_name=datetime.datetime(2018, 1, 1, 0, 0)),
                ),
                ("is_active", models.BooleanField(default=True)),
                ("created", models.DateTimeField(auto_now_add=True)),
                (
                    "city",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.RESTRICT,
                        to="fruit_app.shop",
                    ),
                ),
            ],
        ),
    ]
