from django.contrib import admin
from . import models

from import_export.admin import ImportExportModelAdmin
from import_export import resources


class RawSalesNumbersResource(resources.ModelResource):
    class Meta:
        model = models.FruitStatistics
        fields = (
            "fruit__name",
            "fruit_file__shop__city__name",
            "units_sold",
            "fruit_file__week_date",
        )


# Register your models here.
class RawSalesNumbersAdmin(ImportExportModelAdmin):
    resource_class = RawSalesNumbersResource


admin.site.register(models.User)
admin.site.register(models.Shop)
admin.site.register(models.City)
admin.site.register(models.Fruit)
admin.site.register(models.FruitFile)
admin.site.register(models.FruitStatistics, RawSalesNumbersAdmin)
admin.site.register(models.Overheads)
# admin.site.register(RawSalesNumbersAdmin)
