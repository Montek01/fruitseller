from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.generic.dates import ArchiveIndexView

from . import views
from . import models

urlpatterns = [
    path("", views.home, name="home"),
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("register/", views.RegisterView.as_view(), name="register"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("upload/", views.upload, name="upload"),
    path("csv_export/", views.csv_export, name="csv_export"),
    path("upload/single/", views.upload_single, name="upload_single"),
    path(
        "upload/multiple/",
        views.UploadMultiple.as_view(),
        name="upload_multiple"
    ),
    path("shops/", views.ListShopView.as_view(), name="shop_list"),
    path(
        "shops/create",
        views.CreateShopView.as_view(),
        name="create_shop"
    ),
    path(
        "shops/edit/<int:pk>",
        views.EditShopView.as_view(),
        name="edit_shop"
    ),
    path(
        "shops/delete/<int:pk>",
        views.DeleteShopView.as_view(),
        name="delete_shop"
    ),
    path(
        "shops/view/<int:pk>",
        views.ViewShopView.as_view(),
        name="view_shop"
    ),
    path(
        "initialize_fruit_city_data/",
        views.initialize_fruit_city_data,
        name="initialize_fruit_city_data",
    ),
    path(
        "archive/",
        ArchiveIndexView.as_view(
            model=models.FruitFile,
            date_field="created",
            template_name="fruit_app/archive_spreadsheets.html",
        ),
        name="archive_spreadsheets",
    ),
    path("files/", views.FileListView.as_view(), name="file_list"),
    path(
        "file/source_data/<slug:slug_from_url>/",
        views.FileDetailView.as_view(),
        name="file_source_data",
    ),
    path("overview/", views.overview, name="overview"),
    path("ajax_overview/", views.ajax_overview, name="ajax_overview"),
]

handler404 = "fruit_app.views.custom_404_view"
