from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.text import slugify


import datetime


class User(AbstractUser):
    def __str__(self):
        return f"{self.username}"


class City(models.Model):
    name = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"


class Shop(models.Model):
    name = models.CharField(max_length=100, blank=True)
    city = models.ForeignKey(City, on_delete=models.RESTRICT)
    address = models.TextField(blank=True)
    first_opened = models.DateField(default=datetime.datetime(2018, 1, 1))
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Shop Name:{self.name}, Shop City:{self.city.name}"


class Fruit(models.Model):
    name = models.CharField(max_length=50)
    unit = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"


def get_file_path(instance, filename):
    file_date = filename.split("-")
    return f"{file_date[0]}/{file_date[1]}/{filename}"


class FruitFile(models.Model):
    user = models.ForeignKey(User, on_delete=models.RESTRICT)
    shop = models.ForeignKey(Shop, on_delete=models.RESTRICT)
    week_date = models.DateField(blank=False, null=False)
    file = models.FileField(upload_to=get_file_path)
    slug = models.SlugField(unique=True)
    # TODO: change to False when doing async
    has_processed = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        filename_with_extension = self.file.name.split("/")[-1]
        filename = filename_with_extension.split(".")[0]
        self.slug = self.slug or slugify(filename)
        super().save(*args, **kwargs)

    def __str__(self):
        return f"File:{self.file}, User:{self.user.username}"


class FruitStatistics(models.Model):
    fruit_file = models.ForeignKey(FruitFile, on_delete=models.RESTRICT)
    fruit = models.ForeignKey(Fruit, on_delete=models.RESTRICT)
    units_bought = models.IntegerField(default=0)
    cost_per_unit = models.FloatField(default=0.0)
    units_sold = models.IntegerField(default=0)
    price_per_unit = models.FloatField(default=0.0)
    units_wastage = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Fruit:{self.fruit.name}, Shop:{self.fruit_file.shop.name}"


class Overheads(models.Model):
    fruit_file = models.ForeignKey(FruitFile, on_delete=models.RESTRICT)
    personnel_cost = models.IntegerField(default=0)
    premises_cost = models.IntegerField(default=0)
    other_overheads = models.IntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Shop:{self.fruit_file.shop.name}, " \
               f"Week:{self.fruit_file.week_date}"
