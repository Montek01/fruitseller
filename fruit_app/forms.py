from django.contrib.auth.forms import UserCreationForm
from django import forms

from . import models


class RegisterationForm(UserCreationForm):
    class Meta:
        model = models.User
        fields = ("username", "email")


class UploadSpreadsheetForm(forms.ModelForm):
    class Meta:
        model = models.FruitFile
        fields = ["file"]


class UploadMultipleSpreadsheetForm(forms.Form):
    file = forms.FileField(
        widget=forms.ClearableFileInput(attrs={"multiple": True})
    )


class OverviewFormFilters(forms.Form):
    week_offset = 0
    week_filter = []
    for fruit_file in (
        models.FruitFile.objects.order_by("week_date")
        .all()
        .values("week_date")
        .distinct()
    ):
        if week_offset % 4 == 0:
            week_filter.append(str(fruit_file["week_date"]))
        week_offset += 1
    # TODO instead datepicker?
    start_date = forms.CharField(
        label="Select Start Date",
        widget=forms.Select(choices=[(x, x) for x in week_filter]),
    )
    end_date = forms.CharField(
        label="Select End Date",
        widget=forms.Select(choices=[(x, x) for x in week_filter]),
    )
