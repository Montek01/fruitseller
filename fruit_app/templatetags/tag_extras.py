from django import template
from django.template.defaultfilters import stringfilter

import datetime

register = template.Library()


@register.filter
@stringfilter
def uk_date(date_string):
    return datetime.datetime.strptime(
        date_string, "%Y-%m-%d %H:%M:%S.%f%z"
    ).strftime(
        "%d/%m/%Y"
    )


@register.filter
@stringfilter
def get_absolute_filename(filepath):
    filename_with_extension = filepath.split("/")[-1]
    filename = filename_with_extension.split(".")[0]
    return filename
